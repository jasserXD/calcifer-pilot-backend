FROM adoptopenjdk/openjdk11 
#ADD target/demo-0.0.1-SNAPSHOT.jar /usr/share/app.jar
ADD target/*.jar /usr/share/app.jar
CMD ["java", "-jar", "/usr/share/app.jar"]

#EXPOSE 8080  // revisar esto sobre el documento de buenas practicas
#ENTRYPOINT ["/usr/bin/java", "-jar", "/usr/share/app.jar"]

