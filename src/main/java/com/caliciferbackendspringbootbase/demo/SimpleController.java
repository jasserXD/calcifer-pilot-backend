package com.caliciferbackendspringbootbase.demo;
 
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

 
@Controller
public class SimpleController {
 
    @GetMapping("/")
    @ResponseBody
    public String hola() {
        return "Response from sprintboot";
    }
}